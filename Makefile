CC				=	gcc
CFLAGS			=	-Wall -Wextra -Werror
FSAN			=	-fsanitize=address
RM				=	rm -rf

NAME			=	push_swap
LEAK_CHECK		=	push_swap_leak_check

LIBFT_DIR		=	libft
LIBFT_A			=	libft.a

INCS_DIR		=	incs
INCS			=	push_swap.h

SRCS_DIR		=	srcs
SRCS			=	main.c \
					push_swap.c \
					utils/ft_parse_arguments.c \
					utils/ft_merge_sort.c \
					utils/ft_atoi_array.c \
					utils/ft_has_duplicates.c \
					utils/ft_index_stack.c \
					utils/ft_is_sorted.c \
					utils/ft_small_sort.c \
					utils/ft_radix_sort.c \
					operations/ft_swap.c \
					operations/ft_push.c \
					operations/ft_rotate.c \

OBJS			=	$(addprefix $(SRCS_DIR)/,$(SRCS:.c=.o))

%.o				:	%.c
					$(CC) $(CFLAGS) -I $(INCS_DIR) -c $< -o $@

$(NAME)			:	$(OBJS) $(LIBFT_A)
					$(CC) -o $@ $(OBJS) $(LIBFT_DIR)/$(LIBFT_A)

$(LEAK_CHECK)	:	$(OBJS) $(LIBFT_A)
					$(CC) $(FSAN) -o $@ $(OBJS) $(LIBFT_DIR)/$(LIBFT_A)

$(LIBFT_A)		:
					make -C $(LIBFT_DIR)

all				:	$(NAME)

leak_check		:	$(LEAK_CHECK)

clean			:
					$(RM) $(OBJS)
					make clean -C $(LIBFT_DIR)

fclean			:	clean
					$(RM) $(NAME) $(LEAK_CHECK)
					make fclean -C $(LIBFT_DIR)

re				:	fclean all

.PHONY : all leak_check clean fclean re
