/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 13:52:25 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/19 12:40:42 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <unistd.h>
# include "../libft/libft.h"

typedef struct s_input
{
	int			length;
	int			*array;
}				t_input;

int		ft_parse_arguments(t_input *input, int ac, char **av);
void	ft_merge_sort(int *array, int start, int end);
int		ft_atoi_array(char **str_array, t_input *input);
int		ft_has_duplicates(t_input *input);
int		ft_is_sorted(t_deque *stack);
void	ft_index_stack(t_deque *stack, t_input *input, t_input *holder);
void	ft_swap(t_deque *stack, char *str);
void	ft_push(t_deque *stack_a, t_deque *stack_b, char *str);
void	ft_rotate(t_deque *stack, char *str);
void	ft_reverse_rotate(t_deque *stack, char *str);
void	push_swap(t_deque *stack_a);
void	ft_small_sort(int length, t_deque *stack_a, t_deque *stack_b);
void	ft_radix_sort(int length, t_deque *stack_a, t_deque *stack_b);

#endif
