### Steps:  
1. Read some tutorials:  
	- [LeoFu9487 - push_swap_tutorial](https://github.com/LeoFu9487/push_swap_tutorial)  
	- [Jamie Dawson - Push_Swap: The least amount of moves with two stacks](https://medium.com/@jamierobertdawson/push-swap-the-least-amount-of-moves-with-two-stacks-d1e76a71789a)  


2. Write a merge sort function  
```
srcs ─┬─ utils ─┬─ ft_merge_sort.c
```


3. Create "deque" utilities  
```
libft ─┬─ deque.c
```


4. Create the operations  
```
srcs ─┬─ operations ─┬─ ft_push.c
                     ├─ ft_swap.c
                     └─ ft_rotate.c
```

### How does the program work:  

1. Parse the input into string array  

2. Convert the string array to integer array (and Error return if NaN detected).  
	Put the integer array into the "t_input" struct. Duplicate it for latter use.  

3. Sort the integer array in the "t_input->input_array" using merge sort method.  

4. Index the original array.  
	Use the sorted "input_array" and the duplicated "input_array" to make an indexed "deque" structure as "stack_a".

5. Send the indexed "stack_a" to push_swap.  

### Testers:
	LeoFu9487 : https://github.com/LeoFu9487/push_swap_tester  
	lmalki-h : https://github.com/lmalki-h/push_swap_tester  
	laisarena : https://github.com/laisarena/push_swap_tester  
