/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 15:09:58 by jahuang           #+#    #+#             */
/*   Updated: 2021/11/19 17:51:26 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_free_input(t_input *input)
{
	if (input->array)
		free(input->array);
	free(input);
}

t_input	*ft_init_input(int length)
{
	t_input	*input;

	input = malloc(sizeof(t_input) * 1);
	if (!input)
		return (NULL);
	if (length)
	{
		input->array = malloc(sizeof(int) * length);
		if (!input->array)
		{
			free(input);
			return (NULL);
		}
	}
	else
		input->array = NULL;
	input->length = length;
	return (input);
}

t_input	*ft_dup_input(t_input	*input)
{
	t_input	*input_holder;
	int		index;

	index = 0;
	input_holder = ft_init_input(input->length);
	if (!input_holder)
		return (0);
	while (index < input->length)
	{
		(input_holder->array)[index] = (input->array)[index];
		index++;
	}
	return (input_holder);
}

int	main(int ac, char **av)
{
	t_input	*input;
	t_input	*input_holder;
	t_deque	*stack_a;

	input = ft_init_input(0);
	if (!input)
		return (1);
	if (ft_parse_arguments(input, ac, av) == 1)
	{
		ft_free_input(input);
		return (1);
	}
	input_holder = ft_dup_input(input);
	if (!input_holder)
	{
		ft_free_input(input);
		return (1);
	}
	ft_merge_sort(input->array, 0, input->length - 1);
	stack_a = ft_init_deque();
	if (!stack_a)
		return (1);
	ft_index_stack(stack_a, input, input_holder);
	push_swap(stack_a);
	return (0);
}
