/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 15:10:07 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/19 14:32:19 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_free_stack(t_deque *stack)
{
	t_node	*node_holder;

	node_holder = stack->head;
	while (stack->head)
	{
		stack->head = node_holder->next;
		free(node_holder->content);
		free(node_holder);
		node_holder = stack->head;
	}
	free(stack);
	return ;
}

void	push_swap(t_deque *stack_a)
{
	t_deque	*stack_b;
	int		length;

	length = stack_a->length;
	if (ft_is_sorted(stack_a) == 1)
	{
		ft_free_stack(stack_a);
		return ;
	}
	stack_b = ft_init_deque();
	if (!stack_b)
	{
		ft_free_stack(stack_a);
		return ;
	}
	if (length <= 5)
		ft_small_sort(length, stack_a, stack_b);
	if (length > 5)
		ft_radix_sort(length, stack_a, stack_b);
	ft_free_stack(stack_a);
	ft_free_stack(stack_b);
	return ;
}
