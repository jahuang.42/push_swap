/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_arguments.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 13:45:53 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/25 16:52:56 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static char	**ft_join_array(char **str_array, char *argument)
{
	int		index;
	int		str_arr_len;
	int		arg_arr_len;
	char	**argument_array;
	char	**holder_array;

	str_arr_len = ft_arraylen(str_array);
	argument_array = ft_split(argument, ' ');
	arg_arr_len = ft_arraylen(argument_array);
	holder_array = malloc(sizeof(char *) * (str_arr_len + arg_arr_len + 1));
	index = 0;
	while (str_array[index])
	{
		holder_array[index] = str_array[index];
		index++;
	}
	while (argument_array[index - str_arr_len])
	{
		holder_array[index] = argument_array[index - str_arr_len];
		index++;
	}
	holder_array[index] = 0;
	free(argument_array);
	free(str_array);
	return (holder_array);
}

static char	**ft_arguments_to_array(int ac, char **av)
{
	int		index;
	char	**str_array;

	str_array = ft_split(av[1], ' ');
	index = 2;
	while (index < ac)
	{
		str_array = ft_join_array(str_array, av[index]);
		index++;
	}
	return (str_array);
}

void	ft_free_array(char **array)
{
	int	index;

	index = 0;
	while (array[index])
	{
		free(array[index]);
		index++;
	}
	free(array);
}

int	ft_parse_arguments(t_input *input, int ac, char **av)
{
	char	**str_array;

	if (ac < 2)
		return (1);
	str_array = ft_arguments_to_array(ac, av);
	if (ft_atoi_array(str_array, input) == 1)
	{
		ft_free_array(str_array);
		ft_putstr_fd("Error\n", 2);
		return (1);
	}
	if (ft_has_duplicates(input))
	{
		ft_free_array(str_array);
		ft_putstr_fd("Error\n", 2);
		return (1);
	}
	ft_free_array(str_array);
	return (0);
}
