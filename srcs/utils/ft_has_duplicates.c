/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_has_duplicates.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:05:49 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 14:05:50 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	ft_has_duplicates(t_input *input)
{
	int	index_a;
	int	index_b;

	if (!input)
	{
		ft_putstr("no nbr");
		return (1);
	}
	index_a = 0;
	while (index_a < input->length)
	{
		index_b = index_a + 1;
		while (index_b < input->length)
		{
			if ((input->array)[index_a] == (input->array)[index_b])
				return (1);
			index_b++;
		}
		index_a++;
	}
	return (0);
}
