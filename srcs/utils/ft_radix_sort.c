/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_radix_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:43:53 by jahuang           #+#    #+#             */
/*   Updated: 2021/11/19 17:36:44 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_count_bits(int nbr)
{
	int	bits;

	bits = 0;
	while (nbr)
	{
		nbr >>= 1;
		bits++;
	}
	return (bits);
}

void	ft_radix_sort(int length, t_deque *stack_a, t_deque *stack_b)
{
	int		bits;
	int		round;
	int		count_a;

	bits = ft_count_bits(length);
	round = 0;
	while (round < bits && !ft_is_sorted(stack_a))
	{
		count_a = 0;
		while (count_a < length)
		{
			if ((*(int *)(stack_a->head->content) >> round) % 2 == 0)
				ft_push(stack_a, stack_b, "pb\n");
			else
				ft_rotate(stack_a, "ra\n");
			count_a++;
		}
		while (stack_b->head)
			ft_push(stack_b, stack_a, "pa\n");
		round++;
	}
	return ;
}
