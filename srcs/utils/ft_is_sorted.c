/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sorted.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 15:00:15 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 15:00:59 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	ft_is_sorted(t_deque *stack)
{
	t_node	*holder;
	int		index;

	holder = stack->head;
	index = 0;
	while (index < stack->length - 1)
	{
		if (*(int *)(holder->content) > *(int *)(holder->next->content))
			return (0);
		holder = holder->next;
		index++;
	}
	return (1);
}
