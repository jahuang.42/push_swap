/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_array.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:59:13 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/21 12:19:31 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	is_int(char *str)
{
	int		negatif;
	size_t	result;

	negatif = 1;
	result = 0;
	while ((*str >= '\t' && *str <= '\r') || *str == ' ')
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			negatif = -1;
		str++;
	}
	if (!*str)
		return (0);
	while (*str >= '0' && *str <= '9')
	{
		result = (result * 10) + (*str - 48);
		str++;
	}
	if (negatif < 0 && result > 2147483648)
		return (0);
	if (negatif > 0 && result > 2147483647)
		return (0);
	return (1);
}

int	ft_atoi_array(char **str_array, t_input *input)
{
	int	index;
	int	arraylen;

	arraylen = ft_arraylen(str_array);
	if (str_array[0] == 0)
		return (1);
	input->array = malloc(sizeof(int) * arraylen);
	if (!input->array)
		return (1);
	input->length = arraylen;
	index = 0;
	while (str_array[index])
	{
		if (!ft_isnum(str_array[index]))
		{
			input->length = 0;
			input = NULL;
			return (1);
		}
		if (!is_int(str_array[index]))
			return (1);
		(input->array)[index] = ft_atoi(str_array[index]);
		index++;
	}
	return (0);
}
