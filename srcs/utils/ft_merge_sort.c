/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_merge_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 15:36:27 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 14:42:59 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_move(int *array, int *temp_array, int len)
{
	int	index;

	index = 0;
	while (index < len)
	{
		array[index] = temp_array[index];
		index++;
	}
}

int	ft_copy(int *temp_array, int *array)
{
	if (!temp_array || !array)
		return (0);
	*temp_array = *array;
	return (1);
}

void	ft_merge(int *array, int *temp_array, int start, int end)
{
	int	temp_i;
	int	left_i;
	int	right_i;

	temp_i = 0;
	left_i = start;
	right_i = (start + (end - start) / 2) + 1;
	while (left_i <= (start + (end - start) / 2) && right_i <= end)
	{
		if (array[left_i] < array[right_i])
			left_i += ft_copy(&temp_array[temp_i], &array[left_i]);
		else
			right_i += ft_copy(&temp_array[temp_i], &array[right_i]);
		temp_i++;
	}
	while (right_i <= end || left_i <= (start + (end - start) / 2))
	{
		if (right_i <= end)
			right_i += ft_copy(&temp_array[temp_i], &array[right_i]);
		else
			left_i += ft_copy(&temp_array[temp_i], &array[left_i]);
		temp_i++;
	}
	ft_move(array + start, temp_array, end - start + 1);
}

void	ft_merge_sort(int *array, int left, int right)
{
	int	mid;
	int	*temp_array;

	if (left >= right)
		return ;
	mid = left + (right - left) / 2;
	temp_array = malloc(sizeof(int) * (right - left + 1));
	ft_merge_sort(array, left, mid);
	ft_merge_sort(array, mid + 1, right);
	ft_merge(array, temp_array, left, right);
	if (temp_array)
		free(temp_array);
}
