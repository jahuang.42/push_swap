/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_small_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 15:01:18 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 15:01:19 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_is_min(int nbr, t_deque *stack)
{
	t_node	*holder;

	holder = stack->head;
	while (holder)
	{
		if (nbr > *(int *)(holder->content))
			return (0);
		holder = holder->next;
	}
	return (1);
}

static int	ft_is_max(int nbr, t_deque *stack)
{
	t_node	*holder;

	holder = stack->head;
	while (holder)
	{
		if (nbr < *(int *)(holder->content))
			return (0);
		holder = holder->next;
	}
	return (1);
}

static void	ft_sort_three(t_deque *stack)
{
	int	head_nbr;
	int	tail_nbr;

	head_nbr = *(int *)(stack->head->content);
	tail_nbr = *(int *)(stack->tail->content);
	if (ft_is_sorted(stack) == 1)
		return ;
	if (ft_is_max(head_nbr, stack) == 1)
	{
		if (ft_is_min(tail_nbr, stack) == 1)
			ft_swap(stack, "sa\n");
		else
			ft_reverse_rotate(stack, "rra\n");
	}
	if ((ft_is_min(head_nbr, stack) == 1) || (ft_is_min(tail_nbr, stack) == 1))
		ft_reverse_rotate(stack, "rra\n");
	if (ft_is_max(tail_nbr, stack) == 1)
		ft_swap(stack, "sa\n");
	ft_sort_three(stack);
}

static void	ft_sort_five(t_deque *stack_a, t_deque *stack_b)
{
	while (stack_a->length != 3)
	{
		if (*(int *)(stack_a->head->content) < 2)
			ft_push(stack_a, stack_b, "pb\n");
		else
			ft_rotate(stack_a, "ra\n");
	}
	ft_sort_three(stack_a);
	while (stack_b->length != 0)
		ft_push(stack_b, stack_a, "pa\n");
	if (*(int *)(stack_a->head->content) != 0)
		ft_swap(stack_a, "sa\n");
	return ;
}

void	ft_small_sort(int length, t_deque *stack_a, t_deque *stack_b)
{
	if (length == 2)
		ft_swap(stack_a, "sa\n");
	if (length == 3)
		ft_sort_three(stack_a);
	if (length > 3)
		ft_sort_five(stack_a, stack_b);
	return ;
}
