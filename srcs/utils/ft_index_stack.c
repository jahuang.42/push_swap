/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_index_stack.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:59:51 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/19 12:09:24 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_index_stack(t_deque *stack, t_input *input, t_input *holder)
{
	int		index_a;
	int		index_b;
	int		*number;

	index_b = 0;
	while (index_b < input->length)
	{
		index_a = 0;
		while (index_a < input->length)
		{
			if ((input->array)[index_a] == (holder->array)[index_b])
			{
				number = malloc(sizeof(int *) * 1);
				*number = index_a;
				ft_add_back(stack, (void *)number);
			}
			index_a++;
		}
		index_b++;
	}
	free(input->array);
	free(input);
	free(holder->array);
	free(holder);
}
