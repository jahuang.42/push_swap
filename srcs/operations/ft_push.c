/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:05:01 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 14:46:52 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_push(t_deque *stack_a, t_deque *stack_b, char *str)
{
	void	*content;

	content = ft_pop_front(stack_a);
	ft_add_front(stack_b, content);
	ft_putstr(str);
}
