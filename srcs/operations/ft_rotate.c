/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:04:47 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 14:47:23 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_rotate(t_deque *stack, char *str)
{
	void	*content;

	content = ft_pop_front(stack);
	ft_add_back(stack, content);
	ft_putstr(str);
}

void	ft_reverse_rotate(t_deque *stack, char *str)
{
	void	*content;

	content = ft_pop_back(stack);
	ft_add_front(stack, content);
	ft_putstr(str);
}
