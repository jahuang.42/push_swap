/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:05:15 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 14:46:28 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_swap(t_deque *stack, char *str)
{
	void	*content_a;
	void	*content_b;

	content_a = ft_pop_front(stack);
	content_b = ft_pop_front(stack);
	ft_add_front(stack, content_a);
	ft_add_front(stack, content_b);
	ft_putstr(str);
}
