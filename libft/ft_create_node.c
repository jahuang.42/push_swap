/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_node.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 13:56:38 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 17:02:24 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_node	*ft_create_node(void *content)
{
	t_node	*new_node;

	new_node = malloc(1 * sizeof(t_node));
	new_node->next = NULL;
	new_node->previous = NULL;
	new_node->content = content;
	return (new_node);
}
