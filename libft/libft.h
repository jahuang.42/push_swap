/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 13:59:08 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 13:59:39 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stdlib.h>
# include <stddef.h>
# include <unistd.h>

typedef struct s_node
{
	void			*content;
	struct s_node	*next;
	struct s_node	*previous;
}					t_node;

typedef struct s_deque
{
	t_node			*head;
	t_node			*tail;
	int				length;
}					t_deque;

t_node				*ft_create_node(void *content);
t_deque				*ft_init_deque(void);
void				ft_add_front(t_deque *deque, void *content);
void				ft_add_back(t_deque *deque, void *content);
void				*ft_pop_front(t_deque *deque);
void				*ft_pop_back(t_deque *deque);
void				ft_putstr(char *str);
void				ft_putstr_fd(char *str, int fd);
char				**ft_split(const char *str, char c);
size_t				ft_strlen(const char *str);
char				*ft_substr(const char *str, unsigned int start, size_t len);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_isdigit(int c);
int					ft_isnum(char *str);
void				ft_putnbr_fd(int n, int fd);
size_t				ft_arraylen(char **str_array);
int					ft_atoi(char *str);

#endif
