/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isnum.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 13:57:16 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 15:29:31 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isnum(char *str)
{
	int	index;

	index = 0;
	while (str[index])
	{
		if (index == 0 && \
			str[index] != '+' && \
			str[index] != '-' && \
			ft_isdigit(str[index]) == 0)
			return (0);
		if (index != 0 && \
			ft_isdigit(str[index]) == 0)
			return (0);
		index++;
	}
	return (1);
}
