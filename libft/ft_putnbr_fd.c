/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 13:57:29 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/18 14:04:11 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	int	printn;

	if (n == -2147483648)
	{
		write(fd, "-", 1);
		n = -(n / 10);
		ft_putnbr_fd(n, fd);
		ft_putnbr_fd(8, fd);
		return ;
	}
	if (n < 0)
	{
		write(fd, "-", 1);
		n = -n;
	}
	if ((n / 10) != 0)
	{
		ft_putnbr_fd(n / 10, fd);
	}
	printn = n % 10 + 48;
	write(fd, &printn, 1);
	return ;
}
