/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   deque.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 13:55:11 by jahuang           #+#    #+#             */
/*   Updated: 2021/10/19 14:32:18 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_pop_front(t_deque *deque)
{
	t_node	*holder;
	void	*content;

	if (deque->length < 1 || !(deque->head))
		return (NULL);
	(deque->length)--;
	holder = deque->head;
	deque->head = deque->head->next;
	if (deque->head)
		deque->head->previous = NULL;
	content = holder->content;
	free(holder);
	return (content);
}

void	*ft_pop_back(t_deque *deque)
{
	t_node	*holder;
	void	*content;

	if (deque->length < 1 || !(deque->head))
		return (NULL);
	(deque->length)--;
	holder = deque->tail;
	deque->tail = deque->tail->previous;
	if (deque->tail)
		deque->tail->next = NULL;
	content = holder->content;
	free(holder);
	return (content);
}

void	ft_add_front(t_deque *deque, void *content)
{
	t_node	*holder;

	holder = ft_create_node(content);
	if (!holder)
		return ;
	(deque->length)++;
	if (deque->length == 1)
	{
		deque->head = holder;
		deque->tail = holder;
		return ;
	}
	holder->next = deque->head;
	deque->head->previous = holder;
	deque->head = deque->head->previous;
	return ;
}

void	ft_add_back(t_deque *deque, void *content)
{
	t_node	*holder;

	holder = ft_create_node(content);
	if (!holder)
		return ;
	(deque->length)++;
	if (deque->length == 1)
	{
		deque->head = holder;
		deque->tail = holder;
		return ;
	}
	holder->previous = deque->tail;
	deque->tail->next = holder;
	deque->tail = deque->tail->next;
	return ;
}

t_deque	*ft_init_deque(void)
{
	t_deque	*new_deque;

	new_deque = malloc(1 * sizeof(t_deque));
	new_deque->head = NULL;
	new_deque->tail = NULL;
	new_deque->length = 0;
	return (new_deque);
}
